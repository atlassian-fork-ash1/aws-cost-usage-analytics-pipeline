from __future__ import print_function
from datetime import date
from botocore.exceptions import ClientError, ParamValidationError
import json
import boto3
import logging
import os
import sys
here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "./vendored"))
from dateutil.relativedelta import relativedelta  # noqa
from jinja2 import Environment, FileSystemLoader  # noqa


logging.basicConfig(level=logging.DEBUG)
logging.getLogger("boto3").setLevel(logging.WARNING)
logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger("botocore.auth").setLevel(logging.WARNING)
logging.getLogger("botocore.hooks").setLevel(logging.WARNING)
logging.getLogger("botocore.endpoint").setLevel(logging.WARNING)
logging.getLogger("botocore.client").setLevel(logging.WARNING)
logging.getLogger("botocore.parsers").setLevel(logging.WARNING)


class AwsCostUsagePipelineCreator:
    def __init__(self, config):
        self.config = config
        self.region = config['region']
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel('DEBUG')
        self.s3_client = self._get_boto3_connection('s3')
        self.datapipeline_client = self._get_boto3_connection('datapipeline')

    def _get_boto3_connection(self, service):
        return boto3.client(service_name=service, region_name=self.region)

    def _get_s3_file_last_modified(self, bucket, key):
        try:
            response = self.s3_client.head_object(Bucket=bucket, Key=key)
            if 'LastModified' in response:
                return response['LastModified']
        except ClientError as e:
            if e.response['Error']['Code'] == "404":
                return None
            else:
                print(bucket, key)
                raise e
        return None

    def _get_s3_file_body(self, bucket, key):
        response = self.s3_client.get_object(Bucket=bucket, Key=key)
        if response and 'Body' in response:
            return response['Body'].read()
        return None

    def _generate_manifest_key(self, period, assembly_id=None):
        if assembly_id:
            billing_file_manifest = '{prefix}/{name}/{period}/{assembly_id}/{name}-Manifest.json'.format(
                prefix=self.config['billingReportPrefix'],
                name=self.config['billingReportName'],
                assembly_id=assembly_id,
                period=period)
        else:
            billing_file_manifest = '{prefix}/{name}/{period}/{name}-Manifest.json'.format(
                prefix=self.config['billingReportPrefix'],
                name=self.config['billingReportName'],
                period=period)
        return billing_file_manifest

    def _generate_assembly_key(self, period, assembly_id):
        assembly_key = '{prefix}/{name}/{period}/{assemblyId}/'.format(
            prefix=self.config['billingReportPrefix'],
            name=self.config['billingReportName'],
            period=period,
            assemblyId=assembly_id)
        return assembly_key

    def _get_manifest(self, bucket, key):
        last_modified = self._get_s3_file_last_modified(bucket, key)
        if last_modified:
            manifest = json.loads(self._get_s3_file_body(bucket, key))
            manifest['lastModified'] = last_modified
            return manifest
        else:
            return None

    @staticmethod
    def _get_manifest_input_schema(manifest):
        if 'columns' in manifest:
            header = ''
            for column in manifest['columns']:
                header += '{category}::{name},'.format(category=column['category'],
                                                       name=column['name'].replace(':', '_'))

            return header[:-1]
        return ""

    def _get_periods(self):
        process_periods = list()
        if self.config['currentDay'] < self.config['cloudWatchRetention']:
            process_periods.append(self.config['previousPeriod'])
        else:
            self.logger.info('TODO: Delete previous period datapipeline here')
        process_periods.append(self.config['currentPeriod'])
        return process_periods

    def _get_manifests_to_process(self):
        manifests = list()
        for period in self._get_periods():
            key = self._generate_manifest_key(period)
            manifest = self._get_manifest(self.config['billingFilesS3Bucket'], key)
            if manifest:
                manifest['period'] = period
                manifest['key'] = key
                manifest['inputSchema'] = self._get_manifest_input_schema(manifest)
                previous_run = self._get_previous_run_for_period(period)
                if previous_run:
                    manifest['previousRun'] = previous_run
                manifests.append(manifest)
        return manifests

    def _get_previous_run_for_period(self, period):
        assembly_id = self._get_tracking_file_for_period(period)
        if assembly_id:
            self.logger.info('Loading previous run details')
            # check the tracking file points to a valid billing fileset
            billing_file_key = self._generate_manifest_key(period, assembly_id)
            last_modified = self._get_s3_file_last_modified(self.config['billingFilesS3Bucket'], billing_file_key)
            if last_modified:
                self.logger.info('Found previous run manifest')
                manifest = self._get_manifest(self.config['billingFilesS3Bucket'], billing_file_key)
                if manifest:
                    self.logger.info('Found valid previous run')
                    previous_run = {
                        "assemblyId": assembly_id,
                        "lastModified": last_modified,
                        "manifest": manifest,
                        'inputSchema': self._get_manifest_input_schema(manifest)
                    }
                    return previous_run
                else:
                    self.logger.warn('Billing file manifest: s3://%s/%s empty',
                                     self.config['billingFilesS3Bucket'], billing_file_key)
            else:
                self.logger.warn('Tracking file points to invalid billing fileset: s3://%s/%s',
                                 self.config['billingFilesS3Bucket'], billing_file_key)
        return None

    def _generate_tracking_file_key(self, period):
        last_increment = '{prefix}/{name}/{period}/last_increment'.format(
            prefix=self.config['billingReportPrefix'],
            name=self.config['billingReportName'],
            period=period)
        return last_increment

    def _get_tracking_file_for_period(self, period):
        tracking_file = None
        key = self._generate_tracking_file_key(period)
        last_modified = self._get_s3_file_last_modified(self.config['analyticsS3Bucket'], key)
        if last_modified:
            assembly_id = self._get_s3_file_body(self.config['analyticsS3Bucket'], key).replace('\n', '')
            if assembly_id:
                tracking_file = assembly_id
            else:
                self.logger.warn('Tracking file: s3://%s/%s has no content',
                                 self.config['analyticsS3Bucket'],
                                 key)
        else:
            self.logger.warn('No tracking file found at location: s3://%s/%s',
                             self.config['analyticsS3Bucket'],
                             key)
        return tracking_file

    def _delete_completed_datapipelines(self, pipeline_name):
        matching_pipelines = list()
        paginator = self.datapipeline_client.get_paginator('list_pipelines')
        for pipelines in paginator.paginate():
            if 'pipelineIdList' in pipelines:
                matching_pipelines += [pipeline['id'] for pipeline in pipelines['pipelineIdList']
                                       if pipeline['name'] == pipeline_name]
        if matching_pipelines:
            response = self.datapipeline_client.describe_pipelines(pipelineIds=matching_pipelines)
            for pipeline in response['pipelineDescriptionList']:
                pipeline_healthy = False
                pipeline_complete = False
                for status in pipeline['fields']:
                    if status['key'] == '@healthStatus':
                        if status['stringValue'] == 'HEALTHY':
                            pipeline_healthy = True
                        else:
                            self.logger.info('Refusing to delete matching pipeline %s not in healthy state %s',
                                             pipeline['pipelineId'],
                                             status['stringValue'])
                    if status['key'] == '@pipelineState':
                        if status['stringValue'] == 'FINISHED':
                            pipeline_complete = True
                        else:
                            self.logger.info('Refusing to delete matching pipeline %s in non finished state: %s',
                                             pipeline['pipelineId'],
                                             status['stringValue'])
                if pipeline_healthy and pipeline_complete:
                    self.logger.info('Deleting matching pipeline %s',
                                     pipeline['pipelineId'])
                    self.datapipeline_client.delete_pipeline(pipelineId=pipeline['pipelineId'])
        else:
            self.logger.info('No existing pipelines found for deletion')

    def _create_datapipeline(self, pipeline_name):
        response = self.datapipeline_client.create_pipeline(
            name=pipeline_name,
            uniqueId=pipeline_name,
            description=pipeline_name,
            tags=self.config['pipelineTags']
        )
        if 'pipelineId' in response:
            return response['pipelineId']
        self.logger.error('Pipeline not created with response: %s', response)
        return None

    @staticmethod
    def _get_modules():
        modules_dir = 'modules'
        modules = []
        for o in os.listdir(modules_dir):
            # Skip the base we load this manually below
            # ForEach dir check for base.template in a templates sub-folder
            # Add it to the modules to load
            if o != 'base'\
               and os.path.isdir(os.path.join(modules_dir, o))\
               and os.path.isfile(os.path.join(modules_dir, o, 'templates/base.template')):
                modules.append(os.path.join(o, 'templates/base.template'))
        return modules

    def _get_datapipeline_configuration(self):
        kwargs = {'modules': self._get_modules(),
                  'config': self.config}
        env = Environment(loader=FileSystemLoader('modules'))
        template = env.get_template('base/templates/base.template')
        objects = template.render(**kwargs)
        try:
            pipeline_objects = json.loads(objects)
        except Exception as e:
            self.logger.exception('Error converting template to json: %s, template content: %s', e, objects)
            raise e
        template = env.get_template('base/templates/parameter.template')
        parameter_objects = json.loads(template.render(**kwargs))

        template = env.get_template('base/templates/parameter_values.template')
        parameter_values = json.loads(template.render(**kwargs))
        return pipeline_objects, parameter_objects, parameter_values

    def _configure_datapipeline(self, pipeline_id):
        pipeline_objects, parameter_objects, parameter_values = self._get_datapipeline_configuration()
        try:
            response = self.datapipeline_client.put_pipeline_definition(pipelineId=pipeline_id,
                                                                        pipelineObjects=pipeline_objects,
                                                                        parameterObjects=parameter_objects,
                                                                        parameterValues=parameter_values)
        except ParamValidationError as e:
            self.logger.exception('Pipeline definition has errors %s', e)
            return False
        except Exception as e:
            self.logger.exception('Pipeline configuration error: %s using config: %s, %s, %s',
                                  e, pipeline_objects, parameter_objects, parameter_values)
            return False
        if response['errored']:
            self.logger.error('Pipeline definition has errors %s', response['validationErrors'])
            return False
        if response['validationWarnings']:
            self.logger.warn('Pipeline definition has warnings %s', response['validationWarnings'])
        return True

    def _activate_datapipeline(self, pipeline_id):
        if self.config['dryRun']:
            self.logger.info('Not activating datapipeline in dry run')
            return True
        try:
            response = self.datapipeline_client.activate_pipeline(pipelineId=pipeline_id)
        except ClientError as e:
            self.logger.exception('Failed to activate datapipeline with error: %s', e)
            return False
        if 'ResponseMetadata' in response\
           and 'HTTPStatusCode' in response['ResponseMetadata']\
           and response['ResponseMetadata']['HTTPStatusCode'] == 200:
            self.logger.info('Pipeline %s activated', pipeline_id)
        else:
            self.logger.error('Failed to activate pipeline with response %s', response)
        return True

    def _deploy_datapipeline(self, period):
        pipeline_name = '{name}_{period}'.format(name=self.config['dataPipelineName'], period=period)
        self.logger.info('Deleting any completed datapipelines called: "%s"', pipeline_name)
        self._delete_completed_datapipelines(pipeline_name)

        self.logger.info('Build new datapipeline called: "%s"', pipeline_name)
        pipeline_id = self._create_datapipeline(pipeline_name=pipeline_name)
        if not pipeline_id:
            self.logger.info('No pipeline id found refusing to configure pipeline')
            return False

        self.logger.info('Configure new datapipeline: %s', pipeline_id)
        result = self._configure_datapipeline(pipeline_id=pipeline_id)
        if not result:
            self.logger.error('Datapipeline configuration failed')
            return False

        self.logger.info('Activate datapipeline called: "%s"', pipeline_name)
        return self._activate_datapipeline(pipeline_id=pipeline_id)

    def handler(self):
        manifests = self._get_manifests_to_process()
        if not manifests:
            self.logger('No billing files found for current period')
            return
        for manifest in manifests:
            self.logger.info('Processing manifest for period: %s', manifest['period'])
            if 'previousRun' in manifest:
                if manifest['assemblyId'] != manifest['previousRun']['assemblyId']:
                    self.config['trackingFile'] = self._generate_tracking_file_key(manifest['period'])
                    self.config['newBillingFilesAssemblyId'] = manifest['assemblyId']
                    self.config['billing_loader'] = "base/templates/diff_billing_files_to_previous.template"
                    self.config['oldBillingFiles'] = self._generate_assembly_key(manifest['period'],
                                                                                 manifest['previousRun']['assemblyId'])
                    self.config['newBillingFiles'] = self._generate_assembly_key(manifest['period'],
                                                                                 manifest['assemblyId'])
                    self.config['newBillingFileInputSchema'] = manifest['inputSchema']
                    self.config['oldBillingFileInputSchema'] = manifest['previousRun']['inputSchema']
                    self.logger.info('Run analytics diff between: %s and %s',
                                     self.config['oldBillingFiles'],
                                     self.config['newBillingFiles'])
                    result = self._deploy_datapipeline(manifest['period'])
                    if result:
                        self.logger.info('Pipeline deployed for period %s', manifest['period'])
                    else:
                        self.logger.error('Pipeline not deployed for period %s', manifest['period'])
                else:
                    self.logger.info('Analytics already run for period %s', manifest['period'])
            else:
                self.logger.info('No analytics previously run for period: %s found', manifest['period'])
                self.config['trackingFile'] = self._generate_tracking_file_key(manifest['period'])
                self.config['newBillingFilesAssemblyId'] = manifest['assemblyId']
                self.config['billing_loader'] = "base/templates/stage_whole_billing_file.template"
                self.config['oldBillingFiles'] = None
                self.config['newBillingFiles'] = self._generate_assembly_key(manifest['period'],
                                                                             manifest['assemblyId'])
                self.config['newBillingFileInputSchema'] = manifest['inputSchema']
                self.config['oldBillingFileInputSchema'] = None
                self.logger.info('Run full import for period: %s from %s',
                                 manifest['period'],
                                 self.config['newBillingFiles'])
                result = self._deploy_datapipeline(manifest['period'])
                if not result:
                    self.logger.error('Pipeline not deployed for period %s', manifest['period'])
                else:
                    self.logger.info('Pipeline deployed for period %s', manifest['period'])


def get_config():
    config = dict()
    for key, value in os.environ.iteritems():
        config[key] = value
    config['pipelineTags'] = json.loads(config['pipelineTags'])
    config['currentMonthObj'] = date.today().replace(day=1)
    config['currentDay'] = date.today().day
    config['currentMonth'] = config['currentMonthObj'].month
    config['currentYear'] = config['currentMonthObj'].year
    config['dataPipelineEMREC2RoleInstanceProfileName'] = config['dataPipelineEMREC2RoleInstanceProfile'].split('/')[-1]
    config['dataPipelineEMRRoleName'] = config['dataPipelineEMRRole'].split('/')[-1]
    config['nextMonth'] = config['currentMonthObj']+relativedelta(months=+1)
    config['previousMonth'] = config['currentMonthObj']+relativedelta(months=-1)
    config['currentPeriod'] = '{0}{1:02d}01-{2}{3:02d}01'.format(
                                                config['currentMonthObj'].year,
                                                config['currentMonthObj'].month,
                                                config['nextMonth'].year,
                                                config['nextMonth'].month)
    config['previousPeriod'] = '{0}{1:02d}01-{2}{3:02d}01'.format(
                                                config['previousMonth'].year,
                                                config['previousMonth'].month,
                                                config['currentMonthObj'].year,
                                                config['currentMonthObj'].month)
    config['dryRun'] = config['dryRun'] == 'true'
    config['cloudWatchRetention'] = int(config['cloudWatchRetention'])
    return config


def handler(event, context):
    conf = get_config()
    conf['event'] = event
    conf['context'] = context
    return AwsCostUsagePipelineCreator(conf).handler()


class LoadEnv:
    def __init__(self, stage, region):
        with open("event.json") as conf_file:
            self.event = json.loads(conf_file.read())
        with open('s-function.json') as conf_file:
            self.function_config = conf_file.read()
        with open('../_meta/variables/s-variables-{0}-{1}.json'.format(stage, region.replace('-', ''))) as conf_file:
            self.function_config = self._variable_substitution(json.loads(conf_file.read()), self.function_config)
        self.function_config = self._variable_substitution({'stage': stage}, self.function_config)
        with open('../_meta/variables/s-variables-{0}.json'.format(stage)) as conf_file:
            self.function_config = self._variable_substitution(json.loads(conf_file.read()), self.function_config)
        self.function_config = self._variable_substitution({'stage': stage}, self.function_config)
        with open('../_meta/variables/s-variables-common.json') as conf_file:
            self.function_config = self._variable_substitution(json.loads(conf_file.read()), self.function_config)
        self.function_config = json.loads(self.function_config)
        self.environment = {}
        if 'environment' in self.function_config:
            for key, value in self.function_config['environment'].iteritems():
                if key == 'SERVERLESS_STAGE':
                    value = 'local'
                self.environment[key] = value
                os.environ[key] = value

    @staticmethod
    def _variable_substitution(variables, template):
        for key, value in variables.iteritems():
            template = template.replace('${{{0}}}'.format(key), str(value))
        return template

    def get_env(self):
        return self.environment

    def get_event(self):
        return self.event


if __name__ == '__main__':
    le = LoadEnv('prod', 'us-east-1')
    handler(le.get_event(), {})
