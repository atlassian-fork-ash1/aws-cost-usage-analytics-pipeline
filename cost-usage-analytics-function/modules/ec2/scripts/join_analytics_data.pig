CLOUD_WATCH = load '$CLOUD_WATCH_FILE' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE') as (linkedaccount:chararray,
                                                                                                             instance_id:chararray,
                                                                                                             start_time:chararray,
                                                                                                             cpu_min:chararray,
                                                                                                             cpu_avg:chararray,
                                                                                                             cpu_max:chararray,
                                                                                                             disk_read_min:chararray,
                                                                                                             disk_read_avg:chararray,
                                                                                                             disk_read_max:chararray,
                                                                                                             disk_read_iops_min:chararray,
                                                                                                             disk_read_iops_avg:chararray,
                                                                                                             disk_read_iops_max:chararray,
                                                                                                             disk_write_min:chararray,
                                                                                                             disk_write_avg:chararray,
                                                                                                             disk_write_max:chararray,
                                                                                                             disk_write_iops_min:chararray,
                                                                                                             disk_write_iops_avg:chararray,
                                                                                                             disk_write_iops_max:chararray,
                                                                                                             net_in_min:chararray,
                                                                                                             net_in_avg:chararray,
                                                                                                             net_in_max:chararray,
                                                                                                             net_out_min:chararray,
                                                                                                             net_out_avg:chararray,
                                                                                                             net_out_max:chararray);

COST_REPORT = LOAD '$COST_REPORT_FILE' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE', 'NOCHANGE', 'SKIP_INPUT_HEADER') as ($COST_REPORT_INPUT_SCHEMA);

EC2_USAGE_COSTS = FILTER COST_REPORT BY (lineItem::UsageType matches '.*Usage:.*') AND (lineItem::ProductCode == 'AmazonEC2') AND (lineItem::LineItemType == 'Usage');

RESULT = JOIN EC2_USAGE_COSTS by (lineItem::UsageAccountId,lineItem::UsageStartDate,lineItem::ResourceId), CLOUD_WATCH by (linkedaccount,start_time,instance_id) USING 'replicated';

store RESULT into '$OUTPUT' USING org.apache.pig.piggybank.storage.CSVExcelStorage('\u0001', 'NO_MULTILINE', 'UNIX', 'SKIP_OUTPUT_HEADER');
